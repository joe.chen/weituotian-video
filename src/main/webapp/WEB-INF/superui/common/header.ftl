<#--<#import "common.ftl" as common/>-->

<#macro header title="韦驮天视频">
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>${title}</title>

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<#--<meta content="width=device-width, initial-scale=1" name="viewport"/>-->
<meta content="韦驮天视频" name="description"/>
<meta content="韦驮天" name="author"/>

<!--全局通用框架样式 begin-->

<!-- 全局基本样式 -->
<link href="${common.basePath}/static/superui/min/css/superui.common.min.css" rel="stylesheet"/>
<!-- 全局主题样式 -->

<#--css未压缩版本-->
<link href="${common.AssetsBase}font-awesome/css/font-awesome.css" id="style_components" rel="stylesheet"/>
<link href="${common.AssetsBase}simple-line-icons/simple-line-icons.css" id="style_components" rel="stylesheet"/>
<link href="${common.AssetsBase}bootstrap/css/bootstrap.css" id="style_components" rel="stylesheet"/>
<link href="${common.AssetsBase}uniform/css/uniform.default.css" id="style_components" rel="stylesheet"/>
<link href="${common.AssetsBase}bootstrap-switch/css/bootstrap-switch.css" id="style_components" rel="stylesheet"/>
<link href="${common.AssetsBase}bootstrap-toastr/toastr.css" id="style_components" rel="stylesheet"/>
<#--<link href="${baseCssPath}pace/themes/pace-theme-flash.css" id="style_components" rel="stylesheet"/>-->
<link href="${common.AssetsGlobal}css/plugins.css" id="style_components" rel="stylesheet"/>
<link href="${common.AssetsLayouts}css/layout.css" id="style_components" rel="stylesheet"/>

<#--css压缩后-->
<#--<link href="${basePath}/static/superui/global/css/components.min.css" id="style_components" rel="stylesheet"/>
<link href="${basePath}/static/superui/pages/layouts/css/themes/darkblue.min.css" rel="stylesheet" type="text/css"
      id="style_color"/>-->

<link rel="shortcut icon" href="${common.AssetsGlobal}img/favicon.ico" type=""/>
<link rel="icon" href="${common.AssetsGlobal}img/favicon.ico" type=""/>

<!--全局通用框架样式 end-->
<script src="${common.AssetsBase}jquery/jquery-2.1.1.min.js"></script>

<#nested/>
</#macro>

