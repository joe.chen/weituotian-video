<#--<#import "common.ftl" as common/>-->

<#macro footer>

<!--[if lt IE 9]>
<script src="${common.AssetsGlobal}base-core/excanvas.min.js"></script>
<script src="${common.AssetsGlobal}base-core/respond.min.js"></script>
<![endif]-->

<!-- 全局公共类库Begin -->

<script src="${common.AssetsBase}jquery/jquery-2.1.1.min.js"></script>
<script src="${common.AssetsBase}bootstrap/js/bootstrap.js"></script>
<script src="${common.AssetsBase}jquery-cookie/jquery.cookie.min.js"></script>
<script src="${common.AssetsBase}bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>
<script src="${common.AssetsBase}jquery-slimscroll/jquery.slimscroll.js"></script>
<script src="${common.AssetsBase}bootstrap-switch/js/bootstrap-switch.js"></script>
<script src="${common.AssetsBase}uniform/jquery.uniform.js"></script>

<script src="${common.AssetsBase}jquery-blockui/jquery.blockui.min.js"></script>
<script src="${common.AssetsPages}main/app.js"></script>
<script src="${common.AssetsLayouts}scripts/layout.js"></script>
<script src="${common.AssetsLayouts}scripts/theme.js"></script>
<script src="${common.AssetsLayouts}scripts/quick-sidebar.js"></script>


<script src="${common.AssetsPages}main/sidebarMenu.js"></script>
<script src="${common.AssetsPages}main/bootstrap-tab.js"></script>

<script src="${common.AssetsBase}jquery-pulsate/jquery.pulsate.min.js"></script>
<script src="${common.AssetsBase}jquery-bootpag/jquery.bootpag.js"></script>
<script src="${common.AssetsBase}bootstrap-toastr/toastr.js"></script>

<script src="${common.AssetsBase}bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>
<script src="${common.basePath}/static/superui/min/js/superui.common.min.js"></script>

<#--压缩后js-->
<#--<script src="${header.basePath}/static/superui/min/js/superui.common.min.js"></script>-->
<!--全局公共类库 End-->
</#macro>