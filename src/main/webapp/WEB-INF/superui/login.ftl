<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<#import "common/common.ftl" as common/>
    <title>登录</title>
    <link href="${common.basePath}/static/superui/pages/layouts/css/login.css" rel="stylesheet"/>
    <link rel="icon" href="${common.basePath}/static/superui/global/img/favicon.ico" type=""/>
</head>

<body style="background:#3f4553;background-image: -webkit-gradient(radial,50% 100%,10,50% 50%,1000,from(#3f4553),to(#1f2126));background-image:-moz-radial-gradient(center 80px 45deg, circle farthest-corner, #3f4553 0%, #1f2126 100%);">

<div class="container">
    <section id="content">
        <form id="login_form">
            <h1>韦驮天视频</h1>
            <div>
                <input type="text" placeholder="邮箱" required="" name="username" required/>
            </div>
            <div>
                <input type="password" placeholder="密码" name="password" required/>
            </div>
            <div class="">
                <span class="help-block u-errormessage" id="js-server-helpinfo">&nbsp;</span>
            </div>
            <div>
                <!-- <input type="submit" value="Log in" /> -->
                <input type="submit" value="登录" class="btn btn-primary" id="btn-login"/>
            </div>
        </form><!-- form -->
    <#--<div class="button">
        <span class="help-block u-errormessage" id="js-server-helpinfo">&nbsp;</span>
        <a href="http://www.supermgr.cn">后台管理 </a>
    </div>-->
        <!-- button -->
    </section><!-- content -->
</div>
<!-- container -->

<script src="${common.AssetsBase}jquery/jquery-2.1.1.min.js"></script>
<script src="${common.basePath}/static/plugins/layer/layer.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
        function submitForm() {
            //使用HTML表单来初始化一个FormData对象
            //var data = new FormData($('#myform').get(0));

            $.ajax({
                url: "${common.basePath}/login",
                type: "POST",
                datatype: "json",
                data: $("#login_form").serialize(),
//                processData: false,  // 告诉jQuery不要去处理发送的数据
//                contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
                success: function (data, textStatus) {

                    if (data.success) {
                        window.location.href = data.obj;
                    } else {
                        layer.msg("asd");
                        layer.alert(data.msg, {icon: 3, title: '提示'});
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(XMLHttpRequest + textStatus + errorThrown);
                }
            });
        }

        $('#btn-login').click(function (event) {
            submitForm();
            return false;
        });
    })

</script>
</body>
</html>