<#include "../common/htmlwrap.ftl">
<@html>
    <@header title="${pageName}">

    </@header>

<body>
    <@content_header title='${pageName}'></@content_header>

    <@section_body>
    <form id="search_form" class="form-inline" role="form">
        <div class="form-group">
            <input type="text" class="form-control" name="search_name" id="search_name"
                   value="${(pageinfo.condition['search_name'])!""}"
                   placeholder="名称">
        </div>
        <button id="btn_search" type="submit" class="btn btn-default">搜索</button>
    </form>
    <div id="toolbar">
        <div class="btn-group">
        <#--<a class="btn btn-default lr-replace"><i class="fa fa-refresh"></i>&nbsp;刷新</a>-->
            <a class="btn btn-default lr-add page-action" title="新增资源类型" data-pageId="${add_url}" href="${add_url}"><i
                    class="fa fa-plus"></i>&nbsp;新增</a>
        <#--<a class="btn btn-default lr-edit" onclick="toedit()"><i class="fa fa-pencil-square-o"></i>&nbsp;编辑</a>-->
            <a class="btn btn-default lr-delete" onclick="delete_selection();"><i
                    class="fa fa-remove"></i>&nbsp;删除</a>
            <a class="btn btn-default lr-delete" onclick="selectAll()"><i class="fa fa-remove"></i>&nbsp;全选</a>
            <a class="btn btn-default lr-delete" onclick="reverse_selection()"><i
                    class="fa fa-remove"></i>&nbsp;反选</a>
        <#--<a class="btn btn-default lr-viewlog"><i class="fa fa-detail"></i>&nbsp;查看任务日志</a>-->
        <#--<a class="btn btn-default lr-start"><i class="fa fa-plus"></i>&nbsp;启动</a>-->
        <#--<a class="btn btn-default lr-stop"><i class="fa fa-trash-o"></i>&nbsp;停止</a>-->
        </div>
    </div>
    <table id="table"
           data-toggle="table"
           data-mobile-responsive="true"

           data-row-style="rowStyle2"
           data-toolbar="#toolbar"

           data-show-refresh="true"
           data-show-columns="true"

    <#--点击一行任意地方选择该行-->
           data-click-to-select="true"

    >
        <thead>
        <tr>
            <th data-field="is_checked" data-checkbox="true"></th>
            <th data-field="id" data-sortable="true">id</th>
            <th>名称</th>
            <th data-click-to-select="false">操作</th>
        </tr>
        </thead>
        <tbody>
            <#list pageinfo.list as item>
            <tr>
                <td></td>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-default page-action"
                           data-pageId="resourcetypeedit_${item.id}"
                           href="${edit_url}?id=${item.id}"
                           data-toggle="tooltip" data-placement="bottom" title="修改资源类型${item.id}">
                            修改
                        </a>
                    </div>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
    <div id="pagination">

    </div>
    </@section_body>

    <@footer_list>

    <!--bootstrap表格相关-->
        <@tableCommon delete_url=delete_url></@tableCommon>

    <!--分页js-->
        <@pagination startPage=pageinfo.nowpage totalPages=pageinfo.totalPage></@pagination>

    <script type="text/javascript">
        $(document).ready(function () {

        });
    </script>
    </@footer_list>

</body>

</@html>