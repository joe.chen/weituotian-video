<#macro form_group_text name="" comment="" nullable="NO" >
<div class="form-group">
    <label for="input_${name}">${comment}</label>
    <input type="text" name="${name}" value="${r'$'}{${entity}.${name}}"
           class="form-control" id="input_${name}"
           placeholder=""
           valType=""
        <#if nullable=="NO">
           minlength="2"
           required</#if>
    >
</div>
</#macro>

<#macro form_group_int name="" comment="" nullable="NO" >
<div class="form-group">
    <label for="input_${name}">${comment}</label>
    <input type="number" name="${name}" value="${r'$'}{${entity}.${name}}"
           class="form-control" id="input_${name}"
           placeholder=""
           valType="int"
        <#if nullable=="NO">
           required</#if>
    >
</div>
</#macro>

<#macro form_group_enum>

</#macro>

<#list resState as val>
<li value="${val.getValue()}">${val.getTitle()}</li>
</#list>

<#--垂直表单-->
<#macro form_group_input_ver name="" comment="" nullable="NO" type="varchar">

    <!--${comment}--><#t>
    <#if type=="enum">

    <div class="form-group">
        <label>${comment}</label>
        <div class="checkbox-list">
            <label class="checkbox-inline">
                <input type="checkbox" value="option1"> 多选框 1 </label>
            <label class="checkbox-inline">
                <input type="checkbox" value="option2"> 多选框 2 </label>
            <label class="checkbox-inline">
                <input type="checkbox" value="option3" disabled> 禁用 </label>
        </div>
    </div>

    <#else>

    <div class="form-group">
        <label for="input_${name}">${comment}</label>
        <input name="${name}" value="${r'$'}{${entity}.${name}}"
               class="form-control" id="input_${name}"
               placeholder=""
            <#if type=="varchar">
               type="text"
               valType=""
            <#elseif type=="int">
               type="number"
               valType="number"
            <#elseif type=="datetime">
               type="text"
               valType="date"
               onclick="laydate({elem: 'input_${name}'});"
            </#if>
            <#if nullable=="NO">
               required</#if>>
    </div>
    </#if>

</#macro>

<#--水平表单-->
<#macro form_group_input_hor name="" comment="" nullable="NO" type="varchar">
    <!--${comment}--><#t>
    <#if type=="enum">

    <div class="form-group">
        <label class="col-sm-3 control-label">${comment}</label>
        <div class="checkbox-list col-sm-9">
            <label class="checkbox-inline">
                <input type="checkbox" value="option1"> 多选框 1 </label>
            <label class="checkbox-inline">
                <input type="checkbox" value="option2"> 多选框 2 </label>
            <label class="checkbox-inline">
                <input type="checkbox" value="option3" disabled> 禁用 </label>
        </div>
    </div>

    <#else>

    <div class="form-group">
        <label for="input_${name}" class="col-sm-3 control-label">${comment}</label>
        <div class="col-sm-9">
            <input name="${name}"
                   class="form-control" id="input_${name}"
                   placeholder=""
                <#if type=="varchar">
                   type="text"
                   valType=""
                   value="${r'$'}{${entity}.${name}!""}"
                <#elseif type=="int">
                   type="number"
                   valType="num"
                   value="${r'$'}{${entity}.${name}!0}"
                <#elseif type=="datetime">
                   type="text"
                   valType="date"
                   value="${r'$'}{${entity}.${name}!""?date}"
                   onclick="laydate({elem: 'input_${name}'});"
                </#if>
                <#if nullable=="NO">
                   required</#if>>
        </div>
    </div>

    </#if>

</#macro>



<#list columns as column>
    <@form_group_input_hor

    comment=column.COLUMN_COMMENT
    name=column.COLUMN_NAME
    nullable=column.IS_NULLABLE.getName()
    type=column.DATA_TYPE

    ></@form_group_input_hor>
    <#t>

<#--<#if column.DATA_TYPE=="varchar">
    <@form_group_input comment=column.COLUMN_COMMENT
    name=column.COLUMN_NAME
    nullable=column.IS_NULLABLE.name
    type=column.DATA_TYPE
    ></@form_group_input>
<#elseif column.DATA_TYPE=="int">


<#elseif column.DATA_TYPE=="datetime">

<#elseif column.DATA_TYPE=="enum">

<#else>

</#if>-->
</#list>