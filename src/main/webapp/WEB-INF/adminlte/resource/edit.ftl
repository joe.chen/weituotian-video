<#include "../common/htmlwrap.ftl">

<@html>

    <@header title="${pageName}">

    <#--jstree style-->
    <link rel="stylesheet" href="${common.staticPath}plugins/jstree/dist/themes/default/style.min.css">

    </@header>

<body>

<!-- Content Header (Page header) -->
    <@content_header title='${pageName}'></@content_header>

<section class="content">

    <div class="row">
        <form role="form" action="${form_url}" id="myform" class="form-horizontal">
            <div class="col-sm-12">
                <div class="box box-primary">
                <#--<div class="box-header with-border">
                    <h3 class="box-title">Quick Example</h3>
                </div>-->
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <#if action=='edit'>
                            <!--资源id-->
                            <input name="id" value="${resource.id}"
                                   class="form-control" id="input_id"
                                   placeholder=""
                                   type="hidden"
                                   valType=""
                                   minlength="2"
                                   required>
                        </#if>

                        <!--名字-->
                        <div class="form-group">
                            <label for="input_name" class="col-sm-3 control-label">名字</label>
                            <div class="col-sm-9">
                                <input name="name" value="${resource.name!""}"
                                       class="form-control" id="input_name"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                       required>
                            </div>
                        </div>


                        <!--父资源id-->
                        <div class="form-group">
                            <label for="input_pid" class="col-sm-3 control-label">父资源</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span id="presourcename">${pResource.name}</span>
                                    <input id="input_pid" name="pid" value="${resource.pid}"
                                           class="form-control"
                                           placeholder=""
                                           type="hidden"
                                           valType="num"
                                           required>
                                    <button type="button" onclick="selectParent()" class="btn btn-info">选择父资源</button>
                                </div>
                            </div>
                        </div>

                        <!--地址-->
                        <div class="form-group">
                            <label for="input_url" class="col-sm-3 control-label">地址</label>
                            <div class="col-sm-9">
                                <input name="url" value="${resource.url!""}"
                                       class="form-control" id="input_url"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                       required>
                            </div>
                        </div>


                        <!--描述-->
                        <div class="form-group">
                            <label for="input_description" class="col-sm-3 control-label">描述</label>
                            <div class="col-sm-9">
                                <input name="description" value="${resource.description!""}"
                                       class="form-control" id="input_description"
                                       placeholder=""
                                       type="text"
                                       valType=""
                                >
                            </div>
                        </div>


                        <!--状态-->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">状态</label>
                            <div class="checkbox-list col-sm-9">
                                <#list resStatus as val>
                                    <label class="checkbox-inline">
                                        <input type="radio" name="status" value="${val}"
                                            <#if val.getValue()==resource.status.value>
                                               checked
                                            </#if>
                                        >${val.getTitle()}</label>
                                </#list>
                            <#--<label class="checkbox-inline">
                                <input type="checkbox" value="option1"> 多选框 1 </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="option2"> 多选框 2 </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="option3" disabled> 禁用 </label>-->
                            </div>
                        </div>


                        <!--资源类型id-->
                        <div class="form-group">
                            <label for="input_type_id" class="col-sm-3 control-label">资源类型</label>
                            <div class="col-sm-9">
                                <select name="typeId" class="form-control">
                                    <option value="">资源类型</option>
                                    <#list resTypes as type>
                                        <option value="${type.id}"
                                            <#if type.id==(resource.typeId!-1)> selected</#if>
                                        >${type.name}</option>
                                    </#list>
                                </select>
                            <#--<input name="type_id" value="${resource.typeId}"
                                   class="form-control" id="input_type_id"
                                   placeholder=""
                                   type="number"
                                   valType="num"
                                   required>-->
                            </div>
                        </div>


                        <!--创建时间-->
                        <div class="form-group">
                            <label for="input_create_time" class="col-sm-3 control-label">创建时间</label>
                            <div class="col-sm-9">
                                <input name="create_time" value="${resource.createTime?date}"
                                       class="form-control" id="input_create_time"
                                       placeholder=""
                                       type="text"
                                       valType="date"
                                       onclick="laydate({elem: '#input_create_time'});console.log('laydate');"
                                       required>
                            </div>
                        </div>


                        <!--排序-->
                        <div class="form-group">
                            <label for="input_seq" class="col-sm-3 control-label">排序</label>
                            <div class="col-sm-9">
                                <input name="seq" value="${resource.seq!1}"
                                       class="form-control" id="input_seq"
                                       placeholder=""
                                       type="number"
                                       valType="number"
                                       required>
                            </div>
                        </div>


                        <!--图标-->
                        <div class="form-group">
                            <label for="input_icon" class="col-sm-3 control-label">图标</label>
                            <div class="col-sm-9">
                                <div class="form-inline">
                                    <input name="icon" value="${resource.icon!""}"
                                           class="form-control" id="input_icon"
                                           placeholder=""
                                           type="text"
                                           valType=""
                                           style="min-width: 200px;"
                                    >
                                    <span id="icon_preview" class="${resource.icon!""}"></span>
                                    <button type="button" class="btn btn-primary" onclick="openSelectIcon();">选择图标
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" onclick="close_iframe()" class="btn btn-default">关闭</button>
                        <#if action=="edit">
                            <button type="submit" class="btn btn-info pull-right">保存</button>
                        <#elseif action=="add">
                            <button type="submit" class="btn btn-info pull-right">新增</button>
                        </#if>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">选择父资源</h4>
            </div>
            <div class="modal-body">
                <!--选择树-->
                <div id="res_tree">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <#--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>

    <@footer_edit>

    <!--转换成树的支持-->
    <script src="${common.staticPath}plugins/gtree/tree.js"></script>
    <!--显示js tree-->
    <script src="${common.staticPath}plugins/jstree/dist/jstree.min.js"></script>

    <script type="text/javascript">

        function validform() {
            return $('#myform').Valid();
        }

        function selectParent() {
            /*layer.open({
                type: 1,
                title: false,
                closeBtn: 0,
//                maxWidth: 512,
//                area: '516px',
//                skin: 'layui-layer-nobg', //没有背景色
                shadeClose: true,
                content: $('#res_tree'),
                success: function (index, layero) {
                    if ($(window).width() <= 768) {
                        layer.full(layero);
                    }
                }
            });*/
            $('#myModal').modal("show");
        }

    </script>

    <!--表单-->
    <script>
        $(document).ready(function () {
            //表单
            var $form = $("#myform");

            $form.validatorX();

            $form.submit(function (event) {//$("button[type=submit]")
                if (!$form.Valid()) {
                    event.preventDefault();
                }
            });
        });

    </script>

    <!--显示菜单树-->
    <script>

        var icon_layer_index;

        function setIcon(icon) {
            $("#input_icon").val(icon);
            $("#icon_preview").attr("class", icon);
            layer.close(icon_layer_index);
        }

        function openSelectIcon() {
            icon_layer_index = layer.open({
                type: 2,
                title: '选择图标',
                shadeClose: true,
                shade: false,
                scrollbar: false,
//                maxmin: true, //开启最大化最小化按钮
                area: ['80%', '60%'],
                content: '${common.basePath}/resource/selecticon'
            });
        }

        $(document).ready(function () {

            //如果有错误信息,就显示出来
            var msg = '${msg}';
            if (msg != '') {
                layer.alert(msg);
            }

            //初始化模态框选择
            var $modal = $('#myModal');

            var treeUrl = "${common.basePath}/resource/ajaxtree";

            //jstree数中一项有text,icon和chilren就足够了
            $('#res_tree').jstree({
                'plugins': ["wholerow", "types"],
                'core': {
                    "themes": {
                        "responsive": false
                    },
                    'data': function (node, callback) {
                        console.log("jstree.core.data.function:");
                        console.log(node);
                        $.ajax({
                            url: treeUrl,
                            type: "POST",
                            datatype: "json",
                            //timeout: 3000,
                            data: {
                                "pid": node.id
                            }
                        }).done($.proxy(function (data, status, xhr) {
                            if (data.success) {
                                if (data.obj instanceof Array) {
                                    $.each(data.obj, function (n, value) {
                                        //value.text = value.name;
                                        if (value.childCount > 0) {
                                            value.children = true;
                                        }
                                    });
                                } else if (typeof (data.obj) == "object") {
                                    $.each(data.obj.children, function (n, value) {
                                        //value.text = value.name;
                                        if (value.childCount > 0) {
                                            value.children = true;
                                        }
                                    });
                                }
                                callback(data.obj);
                            } else {
                                layer.alert(data.msg);
                            }
                        }, this)).fail($.proxy(function (XMLHttpRequest, textStatus, errorThrown) {
                            layer.alert(errorThrown);
                        }, this));
                    }
                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder icon-state-warning icon-lg"
                    },
                    "file": {
                        "icon": "fa fa-file icon-state-warning icon-lg"
                    }
                }
            }).bind('select_node.jstree', function (event, selected) {  //绑定的点击事件
                console.log("select_node.jstree");
//                console.log(selected);
                $("#input_pid").val(selected.node.original.id);
                $("#presourcename").text(selected.node.text);

                $modal.modal("hide");
            });

        });
    </script>

    </@footer_edit>

</body>

</@html>