/**
 * Created by ange on 2017/1/18.
 */

var my_skins = [
    "skin-blue",
    "skin-black",
    "skin-red",
    "skin-yellow",
    "skin-purple",
    "skin-green",
    "skin-blue-light",
    "skin-black-light",
    "skin-red-light",
    "skin-yellow-light",
    "skin-purple-light",
    "skin-green-light"
];

function store(name, val) {
    if (typeof (Storage) !== "undefined") {
        localStorage.setItem(name, val);
    } else {
        window.alert('Please use a modern browser to properly view this template!');
    }
}

function get(name) {
    if (typeof (Storage) !== "undefined") {
        return localStorage.getItem(name);
    } else {
        window.alert('Please use a modern browser to properly view this template!');
    }
}

function change_skin(cls) {
    var $body = $("body");
    $.each(my_skins, function (i) {
        $body.removeClass(my_skins[i]);
    });

    $body.addClass(cls);
    $body.css("background-color", "#ecf0f5");
    store('skin', cls);
    return false;
}

var tmp = get('skin');
if (!tmp || !$.inArray(tmp, my_skins)) {
    tmp = my_skins[9];
}
change_skin(tmp);


