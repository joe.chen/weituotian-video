package com.weituotian.mapper;

import com.weituotian.common.utils.PageInfo;
import com.weituotian.model.Register;
import com.weituotian.model.bo.RegisterBo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegisterMapper {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Register record);

    Register selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Register record);

    /**
     * 获得经过分页限制和查询条件的列表
     * @param pageInfo 分页信息
     * @return 列表
     */
    List<RegisterBo> findPageCondition(PageInfo pageInfo);

    List<RegisterBo> findPageCondition2(PageInfo pageInfo);
    /**
     * 获得数量
     * @param pageInfo 分页信息
     * @return 总数
     */
    int findPageCount(PageInfo pageInfo);

    /**
     * 通过id找到挂号BO
     * @return
     */
    RegisterBo findBoById(@Param("id") Integer id);
}