package com.weituotian.common.mybatis.handler.enums;

/**
 *
 * Created by ange on 2017/1/15.
 */
public interface GenericEnum {
    int getCode();
    String getName();
}
