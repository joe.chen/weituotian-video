package com.weituotian.common.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * 通用mapper
 * Created by ange on 2017/1/24.
 */
public interface BaseMapper<T>  {

    int deleteByPrimaryKey(Integer id);

    int insertSelective(T record);

    T selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(T record);

    /**
     * 根据id数组删除条目
     *
     * @return
     */
    int deleteByIds(@Param("ids") Integer[] ids);
}
