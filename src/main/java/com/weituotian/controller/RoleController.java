package com.weituotian.controller;

import com.weituotian.common.controller.BaseController;
import com.weituotian.common.result.JsTreeNode;
import com.weituotian.common.result.Result;
import com.weituotian.common.utils.PageInfo;
import com.weituotian.model.Role;
import com.weituotian.service.IResourceService;
import com.weituotian.service.IRoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色控制器
 * Created by Administrator on 2016-09-26.
 */
@Controller("roleController")
@RequestMapping("/role")
public class RoleController extends BaseController {

    private final IRoleService roleService;

    private final IResourceService resourceService;

    @Autowired
    public RoleController(IRoleService roleService, IResourceService resourceService) {
        this.roleService = roleService;
        this.resourceService = resourceService;
    }

    /**
     * 角色列表
     *
     * @param page 当前页数
     * @return
     */
    @RequestMapping(value = "/list")
    public String list(String search_name, Integer page, Integer pagesize, Model model) {
        PageInfo<Role> pageInfo = new PageInfo<Role>(page, 4);

        //搜索条件
        Map<String, Object> condition = new HashMap<String, Object>();
        if (search_name != null) {
            if (StringUtils.isNoneBlank(search_name)) {
                condition.put("search_name", search_name);
            }
        }
        pageInfo.setCondition(condition);
        roleService.getList(pageInfo);

        model.addAttribute("edit_url", this.getContextPath() + "/role/edit");
        model.addAttribute("add_url", this.getContextPath() + "/role/add");
        model.addAttribute("delete_url", this.getContextPath() + "/role/delete");

        model.addAttribute("pageinfo", pageInfo);
        model.addAttribute("pageName", "角色列表");
        return "role/index";
    }

    /**
     * 编辑页面
     *
     * @return
     */
    @RequestMapping(value = "/edit")
    public String edit(Integer id, Model model) {
        Role role = roleService.findById(id);
        model.addAttribute("role", role);
        model.addAttribute("form_url", this.getContextPath() + "/role/doedit");
        model.addAttribute("action", "edit");

        model.addAttribute("pageName", "角色编辑");
        model.addAttribute("openStatus", roleService.getOpenStatus());
        return "role/edit";
    }

    /**
     * 处理编辑
     *
     * @return
     */
    @RequestMapping(value = "/doedit", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object doedit(Role role) {
        boolean result = roleService.update(role);
        if (result) {
            return renderSuccess("修改成功！");
        }
        return renderError("修改失败！");
    }

    /**
     * 增加页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/add")
    public String add(Model model) {
        Role role = new Role();
        role.setStatus((byte) 1);//默认开启
        role.setSeq(1);

        model.addAttribute("role", role);
        model.addAttribute("form_url", this.getContextPath() + "/role/doadd");
        model.addAttribute("action", "add");

        model.addAttribute("pageName", "角色新增");
        model.addAttribute("openStatus", roleService.getOpenStatus());
        return "role/edit";
    }

    /**
     * 处理增加用户
     *
     * @param role
     * @return
     */
    @RequestMapping(value = "/doadd", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object doadd(Role role) {
        role.setCreateTime(new Date());
        if (roleService.add(role)) {
            return renderSuccess("增加成功!");
        }
        return renderError("增加失败!");
    }

    /**
     * 角色分配资源,返回ajax列表
     *
     * @return
     */
    @RequestMapping(value = "/grant", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object grant(Integer id, Model model) {
        /*List<Resource> all = resourceService.getAllResourceList();*/

        //构造资源树
        /*List<Node<Resource>> resTree = resourceService.findALLTree(all);//所有的资源列表
        List<Integer> resourceIds = resourceService.findResourceIdListByRoleId(id);
        if (resourceIds.size() > 0) {
            for (Node<Resource> node : resTree) {
                if (resourceIds.contains(node.getItem().getId())) {
                    node.setChecked(true);//设置选中
                }
            }
        }*/
        Result result = new Result();
        result.setMsg("获取成功");
        List<JsTreeNode> res = resourceService.getAllResoureceJsTreeByRole(id);
//        model.addAttribute("list", JSONObject.toJSONString(res));
        for (JsTreeNode jsTreeNode : res) {
            if (jsTreeNode.isSelected()) {
                JsTreeNode.State state = jsTreeNode.new State();
                state.selected = true;
                jsTreeNode.setState(state);
            }
        }
        result.setObj(res);
        result.setSuccess(true);

        //角色
//        Role role = roleService.findById(id);

//        model.addAttribute("role", role);
        /*model.addAttribute("resTree", resTree);*/
//        return "role/grant";
        return result;
    }


    /**
     * 处理角色分配资源
     *
     * @return
     */
    @RequestMapping(value = "/dogrant", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object dogrant(Integer roleId, @RequestParam(value = "resIds[]", required = false) Integer[] resIds) {
        try {
            roleService.grant(roleId, resIds);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("分配失败!");
        }
        return renderSuccess("分配成功!");
    }

    /**
     * 批量删除操作
     *
     * @param ids
     * @return
     */
    @RequestMapping(value = "/delete", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Object delete(@RequestParam(value = "ids[]") Integer[] ids) {
        try {
            roleService.deleteByIds(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("删除失败!");
        }
        return renderSuccess("删除成功!");
    }

}
