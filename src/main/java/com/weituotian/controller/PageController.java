package com.weituotian.controller;

import com.weituotian.common.controller.BaseController;
import com.weituotian.common.shiro.ShiroDbRealm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 首页控制器
 * Created by Administrator on 2016-11-16.
 */
@Controller
public class PageController extends BaseController {

    final ShiroDbRealm shiroDbRealm;

    @Autowired
    public PageController(ShiroDbRealm shiroDbRealm) {
        this.shiroDbRealm = shiroDbRealm;
    }

    /**
     * 根
     */
    @RequestMapping("/")
    public String tindex() {
        return "/index";
    }

    /**
     * 首页
     *
     * @return
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index() {
        return "index/index";
    }

    /**
     * 首页的欢迎页面
     *
     * @return
     */
    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String register() {
        return "index/welcome";
    }

    /**
     * 显示信息的页面
     *
     * @return
     */
    @RequestMapping(value = "/msg", method = RequestMethod.GET)
    public String msg(@ModelAttribute("msg") String msg, Model model) {
        model.addAttribute("msg", msg);
        return "/msg";
    }
}
