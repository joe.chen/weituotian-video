package com.weituotian.service;

import com.weituotian.model.Patient;
import com.weituotian.model.ResourceType;

/**
 * 病人服务接口
 * Created by Administrator on 2016-11-16.
 */
public interface IPatientService extends IBaseService<Patient>{

}
