package com.weituotian.service;

import com.weituotian.model.Department;
import com.weituotian.service.impl.BaseService;

import java.util.List;

/**
 *
 * Created by ange on 2016/11/16.
 */
public interface IDepartmentService extends IBaseService<Department>{

    /**
     * 获得所有部门列表
     * @return
     */
    List<Department> getAllList();

}
