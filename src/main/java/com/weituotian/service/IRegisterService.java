package com.weituotian.service;

import com.weituotian.common.utils.PageInfo;
import com.weituotian.model.Register;
import com.weituotian.model.bo.RegisterBo;
import com.weituotian.service.IBaseService;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * Created by ange on 2016/11/17.
 */
public interface IRegisterService extends IBaseService<Register>{
    /**
     * BO分页
     *
     * @param pageInfo 分页信息类
     */
     void getListBo(PageInfo<RegisterBo> pageInfo);

    /**
     * 通过id找到挂号BO
     * @param id
     * @return
     */
    RegisterBo findBoById(Integer id);
}
