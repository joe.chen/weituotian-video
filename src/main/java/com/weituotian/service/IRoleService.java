package com.weituotian.service;

import com.weituotian.common.result.Option;
import com.weituotian.common.utils.PageInfo;
import com.weituotian.model.Role;
import com.weituotian.model.User;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * Created by ange on 2016/9/14.
 */
public interface IRoleService extends IBaseService<Role>{
    /**
     * 获得所有的角色列表
     * @return
     */
    List<Role> getAllRoleList();

    /**
     * 获得所有的角色列表[暂时不用]
     * 传入的用户id拥有的角色会被选择
     * @param userId 用户id
     * @return
     */
    List<Option> getAllRoleListByUser(Integer userId);

    /**
     * 根据用户查询id查询该用户所拥有的角色id列表
     *
     * @param userId 用户id
     * @return
     */
    List<Integer> findRoleIdListByUserId(Integer userId);

    /**
     * 根据用户id,查询该用户的所有角色
     * @param userId 用户id
     * @return
     */
    List<Role> findRolesByUserId(Integer userId);

    /**
     * 为角色分配资源
     * @param roleId 角色id
     * @param resIds 资源id数组
     */
    void grant(Integer roleId, Integer[] resIds);

    /**
     * 从资源实体列表中提取出各个资源的url,组成Set集合
     *
     * @param roles 角色list
     * @return
     */
    Set<String> getRoleNameSet(List<Role> roles);

    /**
     * 获取角色状态map
     * @return
     */
    Map<Integer, String> getOpenStatus();
}
