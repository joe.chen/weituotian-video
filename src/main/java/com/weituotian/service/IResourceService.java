package com.weituotian.service;

import com.weituotian.common.result.JsTreeNode;
import com.weituotian.common.result.Menu;
import com.weituotian.common.result.Node;
import com.weituotian.common.utils.PageInfo;
import com.weituotian.model.Resource;
import com.weituotian.model.vo.ResourceVo;

import java.util.List;
import java.util.Set;

/**
 * 资源接口
 * Created by Administrator on 2016-09-30.
 */
public interface IResourceService extends IBaseService<Resource> {

    /**
     * 获得资源Vo列表
     * @param pageInfo
     */
    void getListVo(PageInfo<ResourceVo> pageInfo);

    /**
     * 主要用于前端jstree的显示
     * @return
     */
    List<ResourceVo> getResourceJsTree(String pid);

    /**
     * 获得所有的资源列表
     *
     * @return
     */
    List<Resource> getAllResourceList();

    /**
     * 获取jstree所有资源
     * @return
     */
    List<JsTreeNode> getAllResoureceJsTreeByRole(Integer roleId);

    /**
     * 找到普通列表的树
     *
     * @param resources
     * @return
     */
    List<Node<Resource>> findALLTree(List<Resource> resources);

    /**
     * 根据角色id查询该角色所拥有的资源id列表
     *
     * @param roleId
     * @return
     */
    List<Integer> findResourceIdListByRoleId(Integer roleId);

    /**
     * 根据角色id查询角色所拥有的资源列表
     *
     * @param roleId
     * @return
     */
    List<Resource> findResourceListByRoleId(Integer roleId);

    /**
     * 根据用户id找到他所拥有的所有资源
     *
     * @param userId 用户id
     * @return
     */
    List<Resource> findResourcesByUserId(Integer userId);

    /**
     * 根据用户id找到该用户所拥有的所有菜单资源
     *
     * @param userId 用户id
     * @return
     */
    List<Resource> findMenusByUserId(Integer userId);

    /**
     * 获得所有菜单资源[超级管理员用]
     *
     * @return
     */
    List<Resource> findMenusAll();

    /**
     * 每一个菜单的url加上contextPath
     * @param res
     * @param contextPath
     */
    void AddContextPath(List<Resource> res, String contextPath);

    /**
     * 将资源list转化为menu的list
     *
     * @param allResources 资源list
     * @return 菜单list
     */
    List<Menu> findAllMenu(List<Resource> allResources);

    /**
     * 从资源实体列表中提取出各个资源的url,组成Set集合
     *
     * @param resources 资源list
     * @return
     */
    Set<String> getResourcesUrlSet(List<Resource> resources);

    /**
     * 一键创建某个控制器的增删查改url
     *
     * @param controller_name 控制器名称
     */
    void autoCreateCRUD(String controller_name, String descript);
}
