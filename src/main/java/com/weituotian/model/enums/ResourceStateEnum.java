package com.weituotian.model.enums;

import com.weituotian.common.mybatis.handler.enums.GenericEnum;

/**
 * 资源状态
 * Created by ange on 2017/1/15.
 */
public enum ResourceStateEnum implements GenericEnum {
    OPEN("open", "开启"), CLOSE("close", "关闭");

    private String value;

    private String title;

    public int getCode() {
        return 0;
    }

    public String getName() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }

    ResourceStateEnum(String value, String title) {
        this.value = value;
        this.title = title;
    }

}
