package com.weituotian.model.vo;

import com.weituotian.model.Resource;

import java.util.List;

/**
 * 资源vo
 * Created by ange on 2017/1/12.
 */
public class ResourceVo extends Resource {

    private String str_resourceType;

    private String str_status;

    private int childCount;

    //与name相同,为了显示在树列表上
    private String text;

    //children列表
    private List<ResourceVo> children;

    //js tree的属性状态
    private State state;

    public class State {
        public boolean opened = false;//是否开启
        public boolean disabled = false;//是否禁用
    }

    public String getStr_resourceType() {
        return str_resourceType;
    }

    public void setStr_resourceType(String str_resourceType) {
        this.str_resourceType = str_resourceType;
    }

    public String getStr_status() {
        return str_status;
    }

    public void setStr_status(String str_status) {
        this.str_status = str_status;
    }

    public int getChildCount() {
        return childCount;
    }

    public void setChildCount(int childCount) {
        this.childCount = childCount;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public List<ResourceVo> getChildren() {
        return children;
    }

    public void setChildren(List<ResourceVo> children) {
        this.children = children;
    }
}
