package com.weituotian.system.service;

import com.weituotian.system.model.Column;

import java.util.List;

/**
 * 代码生成服务
 * Created by ange on 2017/1/15.
 */
public interface IGenerateService {

    List<Column> form(String tableName);

}
