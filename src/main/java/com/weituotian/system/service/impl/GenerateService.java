package com.weituotian.system.service.impl;

import com.weituotian.system.mapper.GenerateMapper;
import com.weituotian.system.model.Column;
import com.weituotian.system.service.IGenerateService;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 代码生成服务
 * Created by ange on 2017/1/15.
 */
@Service("generateService")
public class GenerateService implements IGenerateService {

    protected final Logger logger = LoggerFactory.getLogger(GenerateService.class);

    private final GenerateMapper generateMapper;

    @Value("#{configProperties['jdbc_database']}")
    private String scheme;

    @Autowired
    public GenerateService(GenerateMapper generateMapper) {
        this.generateMapper = generateMapper;
    }


    public List<Column> form(String table) {
        /*Properties pros= PropertyUtil.loadPropertyFile("config.properties");
        pros.get("");*/
        logger.debug("scheme:{}", scheme);
        List<Column> columns = generateMapper.getColumns(scheme, table);
//        logger.debug(ArrayUtils.toString(columns));
        return columns;
    }

}
