package com.weituotian.system.model;

import java.io.Serializable;

/**
 * 数据库的表
 * Created by ange on 2017/1/15.
 */
public class Column implements Serializable {
    //字段名称
    private String COLUMN_NAME;
    //是否为空 NO,Yes
    private NullAbleEnum IS_NULLABLE;
    //数据类型
    private String DATA_TYPE;

    //字符串长度
    private int CHARACTER_MAXIMUM_LENGTH;
    //数字长度
    private int NUMERIC_PRECISION;
    //注释
    private String COLUMN_COMMENT;

    public String getCOLUMN_NAME() {
        return COLUMN_NAME;
    }

    public void setCOLUMN_NAME(String COLUMN_NAME) {
        this.COLUMN_NAME = COLUMN_NAME;
    }

    public NullAbleEnum getIS_NULLABLE() {
        return IS_NULLABLE;
    }

    public void setIS_NULLABLE(NullAbleEnum IS_NULLABLE) {
        this.IS_NULLABLE = IS_NULLABLE;
    }

    public String getDATA_TYPE() {
        return DATA_TYPE;
    }

    public void setDATA_TYPE(String DATA_TYPE) {
        this.DATA_TYPE = DATA_TYPE;
    }

    public int getCHARACTER_MAXIMUM_LENGTH() {
        return CHARACTER_MAXIMUM_LENGTH;
    }

    public void setCHARACTER_MAXIMUM_LENGTH(int CHARACTER_MAXIMUM_LENGTH) {
        this.CHARACTER_MAXIMUM_LENGTH = CHARACTER_MAXIMUM_LENGTH;
    }

    public int getNUMERIC_PRECISION() {
        return NUMERIC_PRECISION;
    }

    public void setNUMERIC_PRECISION(int NUMERIC_PRECISION) {
        this.NUMERIC_PRECISION = NUMERIC_PRECISION;
    }

    public String getCOLUMN_COMMENT() {
        return COLUMN_COMMENT;
    }

    public void setCOLUMN_COMMENT(String COLUMN_COMMENT) {
        this.COLUMN_COMMENT = COLUMN_COMMENT;
    }

    @Override
    public String toString() {
        return "Column{" +
                "COLUMN_NAME='" + COLUMN_NAME + '\'' +
                ", IS_NULLABLE=" + IS_NULLABLE +
                ", DATA_TYPE='" + DATA_TYPE + '\'' +
                ", CHARACTER_MAXIMUM_LENGTH=" + CHARACTER_MAXIMUM_LENGTH +
                ", NUMERIC_PRECISION=" + NUMERIC_PRECISION +
                ", COLUMN_COMMENT='" + COLUMN_COMMENT + '\'' +
                '}';
    }
}
