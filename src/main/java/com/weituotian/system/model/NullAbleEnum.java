package com.weituotian.system.model;

import com.weituotian.common.mybatis.handler.enums.GenericEnum;

/**
 * 是否为空enum
 * Created by ange on 2017/1/15.
 */
public enum NullAbleEnum implements GenericEnum {
    NO("NO"),
    YES("YES");

    private String name;

    public int getCode() {
        return 0;
    }

    public String getName() {
        return name;
    }

    NullAbleEnum(String name) {
        this.name = name;
    }

    public static NullAbleEnum valueOfEnum(String name) {
        NullAbleEnum[] iss = values();
        for (NullAbleEnum cs : iss) {
            if (cs.getName().equals(name)) {
                return cs;
            }
        }
        return null;
    }
}
