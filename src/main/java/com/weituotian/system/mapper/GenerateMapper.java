package com.weituotian.system.mapper;

import com.weituotian.system.model.Column;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 生成代码mapper
 * Created by ange on 2017/1/15.
 */
@Repository
public interface GenerateMapper {

    List<Column> getColumns(@Param("scheme") String scheme, @Param("table") String table);

}
