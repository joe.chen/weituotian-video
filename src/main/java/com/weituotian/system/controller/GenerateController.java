package com.weituotian.system.controller;

import com.weituotian.common.controller.BaseController;
import com.weituotian.common.utils.GenerateUtil;
import com.weituotian.model.enums.ResourceStateEnum;
import com.weituotian.system.model.Column;
import com.weituotian.system.service.IGenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 代码生成
 * Created by ange on 2017/1/15.
 */
@Controller("generateController")
@RequestMapping("/generate")
public class GenerateController extends BaseController {

    private final IGenerateService generateService;

    @Autowired
    public GenerateController(IGenerateService generateService) {
        this.generateService = generateService;
    }

    /**
     * 批量生成表单,基于FreeMarker,AdminLte
     *
     * @param tableName
     */
    @RequestMapping(value = "/form/{tableName}/{entity}")
    public String form(@PathVariable String tableName, @PathVariable String entity, Model model) {
        List<Column> columns = generateService.form(tableName);
        for (Column col : columns) {
            //字段名下划线转换成驼峰形式
            String hump = GenerateUtil.lineToHump(col.getCOLUMN_NAME());
            col.setCOLUMN_NAME(hump);
        }
        model.addAttribute("entity", entity);
        model.addAttribute("columns", columns);
        model.addAttribute("resState", ResourceStateEnum.values());
        return "generate/form";
    }


}
