package com.weituotian.service;


import com.alibaba.druid.util.StringUtils;
import com.weituotian.base.BaseTest;
import com.weituotian.mapper.ResourceMapper;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.List;

public class ResourceServiceTest extends BaseTest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private ResourceMapper resourceMapper;

    @Test
    public void testMenu() {
        List<com.weituotian.model.Resource> resources = resourceMapper.getAllMenus();
        System.out.println(toJson(resources));
        resources = resourceMapper.getAllMenus();
        System.out.println(toJson(resources));
    }

    @Test
    public void testResourceTree(){
        logger.debug(ArrayUtils.toString(resourceMapper.getResourceJsTree(0)));
    }
}
