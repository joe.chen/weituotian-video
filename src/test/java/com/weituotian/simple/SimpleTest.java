package com.weituotian.simple;

import com.weituotian.base.BaseTest;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 基础测试
 * Created by ange on 2016/11/13.
 */
public class SimpleTest extends BaseTest {

    @Test
    public void testSimple() {
        Pattern pattern = Pattern.compile("/user[/.]*");
        Matcher m = pattern.matcher("/user/qwe"); //除中文不用外，其他的都要
        if (m.find()) {
            System.out.println(m.find());
            System.out.println(m.start());
            System.out.println(m.end());
            System.out.println(m.group());
        }
    }
}
